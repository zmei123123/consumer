<?php

use yii\db\Migration;

/**
 * Class m201209_134221_init
 */
class m201209_134221_init extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('queue', [
            'id' => $this->primaryKey(),
            'transaction_id' => $this->integer()->null(),
            'user_id' => $this->integer()->null(),
            'sum' => $this->double()->null(),
        ]);
        $this->createIndex('idx_queue_transaction_id', 'queue', 'transaction_id', true);
        $this->createIndex('idx_queue_user_id', 'queue', 'user_id');

        $this->createTable('user_wallet', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->null(),
            'sum' => $this->double()->null(),
        ]);

        $this->createIndex('idx_user_wallet', 'user_wallet', 'user_id');
        $this->addForeignKey('fk-queue-user_wallet', 'queue', 'user_id', 'user_wallet', 'id', 'SET NULL');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201209_134221_init cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201209_134221_init cannot be reverted.\n";

        return false;
    }
    */
}
