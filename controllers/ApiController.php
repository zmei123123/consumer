<?php

namespace app\controllers;

use app\models\ProcessForm;
use Yii;
use yii\web\BadRequestHttpException;
use yii\filters\VerbFilter;

class ApiController extends \yii\rest\Controller
{

    const VERIFY_TOKEN = '098f6bcd4621d373cade4e832627b4f6';

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'load' => ['post'],
                ],
            ]
        ];
    }

    public function beforeAction($action)
    {
        if (self::VERIFY_TOKEN != Yii::$app->request->headers->get('Authorization')) {
            throw new BadRequestHttpException('Bad request');
        }

        return parent::beforeAction($action);
    }

    public function actionLoad()
    {
        $form = new ProcessForm([
            'data' => Yii::$app->request->getRawBody()
        ]);
        if ($form->validate()) {
            $result = $form->process();
            if ($result) {
                return $this->asJson([
                    'status' => 'ok'
                ]);
            }

            return $this->asJson([
                'status' => 'fail',
                'errors' => $form->getErrors()
            ]);
        }

        return $this->asJson([
            'status' => 'fail'
        ]);
    }

}
