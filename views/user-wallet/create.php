<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UserWallet */

$this->title = 'Create User Wallet';
$this->params['breadcrumbs'][] = ['label' => 'User Wallets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-wallet-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
