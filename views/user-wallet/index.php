<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserWalletSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Wallets';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-wallet-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create User Wallet', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_id',
            'sum',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
