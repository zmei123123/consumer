<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\Exception;

/**
 * LoginForm is the model behind the login form.
 *
 * @property-read User|null $user This property is read-only.
 *
 */
class ProcessForm extends Model
{

    public $allowedItemFields = [
        'id',
        'sum',
        'comission',
        'order_number'
    ];
    public $data;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['data'], 'required'],
            ['data', 'validateData'],
        ];
    }

    public function validateData()
    {
        $result = true;
        $data = json_decode($this->data, true);

        if (json_last_error() != JSON_ERROR_NONE) {
            $result = false;
            $this->addError('data', 'Ошибка в json!');

            return $result;
        }
        foreach ($data as $key => $item) {
            if (!empty(array_diff(array_keys($item), $this->allowedItemFields))) {
                $this->addError('data', 'Ошибка в элементе №' . $key . '!');
                $result = false;
            }
        }
        if ($result) {
            $this->data = $data;
        }

        return $result;
    }

    public function process()
    {
        $result = true;
        foreach ($this->data as $key => $item) {
            $item['sum'] *= (1 - $item['comission']);
            $queue = new Queue([
                'transaction_id' => $item['id'],
                'user_id' => $item['order_number'],
                'sum' => $item['sum']
            ]);
            try {
                if (!$queue->save()) {
                    $result = false;
                    $this->addError('data', 'Неверная запись №' . $key);
                    continue;
                }
            } catch (Exception $e) {
                $result = false;
                $this->addError('data', 'Транзакция уже существует №' . $key);
                continue;
            }
            $user = UserWallet::findOne(['user_id' => $queue->user_id]);
            if (!$user) {
                $user = new UserWallet([
                    'user_id' => $queue->user_id,
                    'sum' => 0
                ]);
            }
            $user->sum += $queue->sum;
            if (!$user->save()) {
                $result = false;
                $this->addError('data', 'Ошибка при сохранении пользователя №' . $key);
                continue;
            }
        }

        return $result;
    }

}
